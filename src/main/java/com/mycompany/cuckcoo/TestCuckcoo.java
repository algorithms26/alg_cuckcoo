/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cuckcoo;

/**
 *
 * @author admin
 */
public class TestCuckcoo {

    public static void main(String[] args) {
        Cuckcoo cuckcoo = new Cuckcoo();

        cuckcoo.input(200, "Jaemin");
        cuckcoo.input(13, "Nana");

        System.out.println(cuckcoo.get(200).toString());
        System.out.println(cuckcoo.get(13).toString());

        cuckcoo.delete(13);
        System.out.println(cuckcoo.get(13));
    }
}
