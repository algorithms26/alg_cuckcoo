/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.cuckcoo;

/**
 *
 * @author admin
 */
public class Cuckcoo {

    private int key;
    private String value;
    private Cuckcoo[] table = new Cuckcoo[83];
    private Cuckcoo[] table1 = new Cuckcoo[83];

    public Cuckcoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckcoo() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hashing(int key) {
        return key % table.length;
    }

    public int hashing1(int key) {
        return (key / 83) % table1.length;
    }

    public void input(int key, String value) {

        if (table[hashing(key)] != null && table[hashing(key)].key == key) {
            table[hashing(key)].key = key;
            table[hashing(key)].value = value;
            return;
        }
        if (table1[hashing1(key)] != null && table1[hashing1(key)].key == key) {
            table1[hashing1(key)].key = key;
            table1[hashing1(key)].value = value;
            return;
        }
        int i = 0;

        //repeat
        while (true) {
            if (i == 0) {
                if (table[hashing(key)] == null) {
                    table[hashing(key)] = new Cuckcoo();
                    table[hashing(key)].key = key;
                    table[hashing(key)].value = value;
                    return;
                }
            } else {
                if (table1[hashing1(key)] == null) {
                    table1[hashing1(key)] = new Cuckcoo();
                    table1[hashing1(key)].key = key;
                    table1[hashing1(key)].value = value;
                    return;
                }
            }

            //eviction
            if (i == 0) {
                String trashValue = table[hashing(key)].value;
                int trashKey = table[hashing(key)].key;
                table[hashing(key)].key = key;
                table[hashing(key)].value = value;
                key = trashKey;
                value = trashValue;

            } else {
                String trashValue = table1[hashing1(key)].value;
                int trashKey = table1[hashing1(key)].key;
                table1[hashing1(key)].key = key;
                table1[hashing1(key)].value = value;
                key = trashKey;
                value = trashValue;
            }
            i = (i + 1) % 2;
        }
    }

    public Cuckcoo get(int key) {
        if (table[hashing(key)] != null && table[hashing(key)].key == key) {
            return table[hashing(key)];
        }
        if (table1[hashing(key)] != null && table1[hashing(key)].key == key) {
            return table1[hashing(key)];
        }
        return null;
    }

    public Cuckcoo delete(int key) {
        if (table[hashing(key)] != null && table[hashing(key)].key == key) {
            Cuckcoo trash = table[hashing(key)];
            table[hashing(key)] = null;
            return trash;
        }
        if (table1[hashing1(key)] != null && table1[hashing1(key)].key == key) {
            Cuckcoo trash = table1[hashing1(key)];
            table1[hashing1(key)] = null;
            return trash;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Key: " + key + " Value: " + value;
    }
}
